{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoDuplicateRecordFields #-}

module SimpleSCF.STO (
    STO,
    mkSTO,
    evalSTO,
    DerivOrd (..),
    int1eSph,
    ovlp,
    kinetic,
    nucattr,
    eri,
    ScfIntegrals (..),
    mkScfIntegrals,
)
where

import Data.Aeson (FromJSON, ToJSON)
import Data.Massiv.Array as Massiv hiding (product)
import Data.Vector.Unboxed.Deriving
import RIO hiding (Vector)
import RIO.Partial (fromJust)
import SimpleSCF.Math
import System.IO.Unsafe (unsafePerformIO)

-- | Exceptions related to STOs
newtype StoException = StoException String deriving (Eq)

instance Show StoException where
    show (StoException msg) = "StoException: " <> msg

instance Exception StoException

-- | Derivative order for evaluation of properties
data DerivOrd
    = Zero
    | First
    | Second
    deriving (Eq, Show, Enum)

-- | A Slater-type orbital. Should always be constructed via `mkSTO`.
data STO = STO
    { n :: Int
    , zeta :: Double
    }
    deriving (Eq, Show, Generic)

instance ToJSON STO
instance FromJSON STO

derivingUnbox
    "STO"
    [t|(Unbox Int, Unbox Double) => STO -> (Int, Double)|]
    [|\STO{..} -> (n, zeta)|]
    [|\(n, zeta) -> STO{..}|]

-- | Smart constructor for STOs
mkSTO :: MonadThrow m => Int -> Double -> m STO
mkSTO n zeta
    | n < 1 = throwM $ StoException "quantum number n must be at least 1"
    | zeta <= 0 = throwM $ StoException "exponent zeta must be greater than 0"
    | otherwise = return $ STO n zeta

-- | Evaluate the STO at some point \(r\).
evalSTO :: STO -> DerivOrd -> Double -> Double
evalSTO STO{..} derivOrd r = case derivOrd of
    Zero -> sto
    First -> ((n' - 1) / r - zeta) * sto
    Second -> (n' * (n' - 1) / r ^ (2 :: Int) - 2 * n' * zeta / r + zeta ^ (2 :: Int)) * sto
  where
    norm = (2 * zeta) ^ n * sqrt (2 * zeta / product [1 .. 2 * n'])
    sto = norm * r ^ (n - 1) * exp (-zeta * r)
    n' = fromIntegral n

-- | Integration of a function in radial coordinates.
int1eSph :: (Double -> Double) -> Double
int1eSph fn = fst . quadInf 0 $ \r -> fn r * r ^ (2 :: Int)

{- | Overlap integrals

\[
\langle \chi_1(r) | \chi_2(r) \rangle
\]
-}
ovlp :: STO -> STO -> Double
ovlp a b = int1eSph $ \r -> evalSTO a Zero r * evalSTO b Zero r

{- | Kinetic integrals

\[
- \frac{1}{2} \langle \chi_1(r) | \nabla^2 | \chi_2(r) \rangle
\]
-}
kinetic :: STO -> STO -> Double
kinetic a b = ((-0.5) *) . int1eSph $ \r -> evalSTO a Zero r * evalSTO b Second r

{- | Nuclear attraction integral

\[
-Z \langle \chi_1(r) | 1/r | \chi_2 (r) \rangle
\]
-}
nucattr :: Natural -> STO -> STO -> Double
nucattr z a b = ((-1) *) . (fromIntegral z *) . int1eSph $ \r ->
    evalSTO a Zero r * evalSTO b Zero r / r

{- | Electron repulsion integrals

\[
( \chi_1(r) \chi_2(r) | 1/r | \chi_3(r) \chi_4(r) )
\]
-}
eri :: STO -> STO -> STO -> STO -> Double
eri a b c d = y1 + y2
  where
    term1 :: Double -> Double
    term1 r =
        let (y, _abserr) = quad 0 r $ \r1 -> evalSTO a Zero r1 * evalSTO b Zero r1 * r1 ** 2
         in evalSTO c Zero r * evalSTO d Zero r * r * y

    term2 :: Double -> Double
    term2 r =
        let (y, _abserr) = quadInf r $ \r1 -> evalSTO a Zero r1 * evalSTO b Zero r1 * r1
         in evalSTO c Zero r * evalSTO d Zero r * r ** 2 * y

    (y1, _abserr1) = quadInf 0 term1
    (y2, _abserr2) = quadInf 0 term2

-- | The required integrals for an SCF
data ScfIntegrals = ScfIntegrals
    { overlapMat :: Matrix S Double
    -- ^ Overlap matrix \(\mathbf{S}\)
    , kineticMat :: Matrix S Double
    -- ^ Kinetic energy matrix \(\mathbf{T}\)
    , nucattractionMat :: Matrix S Double
    -- ^ Nuclear attraction matrix \(\mathbf{Q}\)
    , coreMat :: Matrix S Double
    -- ^ Core Hamiltonian \(\mathbf{H} = \mathbf{T} + \mathbf{Q}\)
    , eriArr :: Array S Ix4 Double
    -- ^ The electron-repulsion integrals over 4 basis functions
    , orthoMat :: Matrix S Double
    }

-- | A basis is a list of STOs.
type Basis = [STO]

-- | Calculate the necessary integrals for an SCF.
mkScfIntegrals :: Natural -> Basis -> ScfIntegrals
mkScfIntegrals z basis = ScfIntegrals{..}
  where
    nBas = length basis

    -- Make a vector of basis functions, pairs of all basis functions and
    -- quadruples of all basis functions
    bfVec :: Vector U STO
    bfVec = fromList Par basis

    bfMat :: Matrix D STO
    bfMat = expandOuter (Sz nBas) const bfVec

    bf2mat :: Matrix D (STO, STO)
    bf2mat = Massiv.zip bfMat . transpose $ bfMat

    bf4Arr :: Array D Ix4 (STO, STO, STO, STO)
    bf4Arr =
        let sz = Sz $ nBas :> nBas :> nBas :. nBas
            d1 = backpermute' sz (\(_ :> _ :> _ :. i) -> i) bfVec
            d2 = backpermute' sz (\(_ :> _ :> i :. _) -> i) bfVec
            d3 = backpermute' sz (\(_ :> i :> _ :. _) -> i) bfVec
            d4 = backpermute' sz (\(i :> _ :> _ :. _) -> i) bfVec
         in Massiv.zip4 d1 d2 d3 d4

    -- 1-Electron integrals
    overlapMat = compute @S $ Massiv.map (uncurry ovlp) bf2mat
    kineticMat = compute @S $ Massiv.map (uncurry kinetic) bf2mat
    nucattractionMat = compute @S $ Massiv.map (uncurry $ nucattr z) bf2mat
    coreMat = kineticMat !+! nucattractionMat

    -- 2-electron integrals
    eriArr :: Array S Ix4 Double
    eriArr = unsafePerformIO . iforIO bf4Arr $ \(_ixA :> _ixB :> _ixC :. _ixD) (a, b, c, d) -> do
        return $ eri a b c d

    -- Orthogonalisation matrix for the basis
    orthoMat :: Matrix S Double
    orthoMat = fromJust $ do
        (s :: Vector U Double, u :: Matrix U Double) <- eigSH overlapMat
        let s12 = diag @U . compute @U $ Massiv.map (\e -> 1 / sqrt e) s
        l :: Matrix U Double <- u ## s12
        l ## (compute @U . transpose) u