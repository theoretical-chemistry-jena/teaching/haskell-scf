{-# LANGUAGE LambdaCase #-}

module SimpleSCF.SCF (scfStep, scf, guessHcore, scfMain) where

import Data.Aeson
import Data.Massiv.Array as Massiv
import RIO hiding (Vector)
import qualified RIO.List as List
import SimpleSCF.Math
import SimpleSCF.STO
import System.Environment (getArgs)
import Prelude (toEnum)

-- | The chemical elements we can handle
data Element = He | Be deriving (Eq, Show, Generic)

instance Enum Element where
  fromEnum He = 2
  fromEnum Be = 4

  toEnum 2 = He
  toEnum 4 = Be
  toEnum _ = error "No enum for element"

instance ToJSON Element
instance FromJSON Element

-- | Input data required for the SCF
data InputData = InputData
  { element :: Element
  , basis :: [STO]
  }
  deriving (Eq, Show, Generic)

instance ToJSON InputData
instance FromJSON InputData

-- | Data produced and/or required by SCF cycles.
data ScfCycle = ScfCycle
  { densityMat :: Matrix S Double
  , energy :: Maybe Double
  , eDiff :: Maybe Double
  , fockMat :: Maybe (Matrix S Double)
  , coeffMat :: Maybe (Matrix S Double)
  }

-- | Perform a single SCF step
scfStep ::
  forall env m.
  (HasLogFunc env, MonadReader env m, MonadIO m, MonadThrow m) =>
  ScfIntegrals ->
  -- | The number of electrons in the system
  Natural ->
  -- | Guess density matrix respective density matrix of the last step
  ScfCycle ->
  m ScfCycle
scfStep ScfIntegrals{..} nEl ScfCycle{..} = do
  logDebug $ "P = " <> displayShow densityMat
  -- Step 1: Calculate matrix G using the density matrix and ERIs.
  -- The matrix-4d ERI array product is instead written as a vector-matrix
  -- product and the result vector transformed back to a matrix.
  let pMatLS = flatten densityMat
      Sz (mU :> mV :> mS :. mL) = Massiv.size eriArr
      eriUVSL =
        backpermute'
          (Sz $ mU * mV :. mS * mL)
          (\(uv :. sl) -> uv `div` mV :> uv `mod` mV :> sl `mod` mS :. sl `div` mS)
          eriArr
      Sz (nU :> nL :> nS :. nV) = Massiv.size eriArr
      eriULSV =
        backpermute'
          (Sz $ nU * nV :. nL * nS)
          (\(uv :. ls) -> uv `div` nS :> ls `div` nV :> ls `mod` nV :. uv `mod` nS)
          eriArr
  gMat <- do
    gVec1 :: Vector S Double <- compute @U eriUVSL #> pMatLS
    gVec2 :: Vector S Double <- compute @U eriULSV #> pMatLS
    let gVec = gVec1 !-! 0.5 *. gVec2
    resizeM (Sz $ mU :. mV) gVec
  logDebug $ "G = " <> displayShow gMat

  -- Step 2: Make the Fock matrix from G + H
  logDebug $ "H = " <> displayShow coreMat
  logDebug $ "X = " <> displayShow orthoMat
  let newFockMat = gMat !+! coreMat
  logDebug $ "F = " <> displayShow newFockMat

  -- Step 3: Transform Fock matrix into orthogonal basis and diagonalise
  orthoFockMat <- (compute . transpose $ orthoMat) ##~ newFockMat >>= (##~ orthoMat)
  logDebug $ "F_ = " <> displayShow orthoFockMat
  (eps, coeffs') <- eigSH' orthoFockMat
  coeffs <- orthoMat ##~ coeffs'
  logDebug $ "epsilon  = " <> displayShow eps <> "\n" <> "C = " <> displayShow coeffs

  -- Step 4: Make a new density matrix from the occupied orbitals
  coeffOcc <- compute <$> extractM (0 :. 0) (Sz $ pR :. fromIntegral nEl `div` 2) coeffs
  newPMat <- (2 *.) <$> coeffOcc ##~ (compute . transpose $ coeffOcc)
  logDebug $ "P_new = " <> displayShow newPMat

  -- Step 5: Calculate the total energy of the system
  let eTot = (0.5 *) . Massiv.sum $ newPMat !*! (compute @S . transpose $ coreMat !+! newFockMat)
  logInfo $ "E_tot = " <> display eTot

  return
    ScfCycle
      { densityMat = newPMat
      , energy = Just eTot
      , eDiff = Just $ fromMaybe 0 energy - eTot
      , fockMat = Just newFockMat
      , coeffMat = Just coeffs
      }
 where
  Sz (pR :. _pC) = size densityMat
  (##~) :: Matrix S Double -> Matrix S Double -> m (Matrix S Double)
  (##~) = (##)
  eigSH' :: Matrix S Double -> m (Vector S Double, Matrix S Double)
  eigSH' = eigSH

-- | Perform SCF steps until convergence is reached.
scf ::
  forall env m.
  (HasLogFunc env, MonadReader env m, MonadIO m, MonadThrow m) =>
  -- | Basis function integrals
  ScfIntegrals ->
  -- | Number of electrons
  Natural ->
  -- | Guess density matrix
  Matrix S Double ->
  -- | Convergence threshold as energy difference
  Double ->
  m ScfCycle
scf ints nEl guess thrs = go initCycle
 where
  go lst = do
    nxt@ScfCycle{..} <- scfStep ints nEl lst
    let conv = (< thrs) . abs <$> eDiff
    if fromMaybe False conv
      then return nxt
      else go nxt

  initCycle =
    ScfCycle
      { densityMat = guess
      , energy = Nothing
      , eDiff = Nothing
      , fockMat = Nothing
      , coeffMat = Nothing
      }

-- | Use eigenvalues of HCore as guess
guessHcore :: MonadThrow m => Natural -> ScfIntegrals -> m (Matrix S Double)
guessHcore nEl ScfIntegrals{coreMat, overlapMat} = do
  (_ :: Vector S Double, coeffs :: Matrix S Double) <- geigSH coreMat overlapMat
  let Sz (cR :. _) = size coeffs
  coeffsOcc <- compute @S <$> extractM (0 :. 0) (Sz $ cR :. fromIntegral nEl `div` 2) coeffs
  (2 *.) <$> coeffsOcc ## (compute @S . transpose $ coeffsOcc)

{- | The main function reads the data file, calculates the integrals, makes the
guess and then converges the SCF.
-}
scfMain :: (HasLogFunc env, MonadReader env m, MonadIO m, MonadThrow m) => m ()
scfMain = do
  args <- liftIO getArgs
  inpFP <- case List.headMaybe args of
    Nothing -> throwString "Give a JSON input file as first argument"
    Just inpFP' -> return inpFP'
  InputData{..} <-
    liftIO $
      decodeFileStrict @InputData inpFP >>= \case
        Nothing -> throwString "Can not read input data"
        Just d -> return d
  let nEl = fromIntegral . fromEnum $ element
      ints = mkScfIntegrals nEl basis
  guess <- guessHcore nEl ints
  void $ scf ints nEl guess 1.0e-6