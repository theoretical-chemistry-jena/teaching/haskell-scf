module SimpleSCF.Math (
  quadInf,
  quad,
  (<.>),
  (#>),
  (##),
  diag,
  eig,
  geig,
  eigSH,
  geigSH,
) where

import Data.Complex (Complex)
import Data.Massiv.Array as Massiv hiding (all)
import Data.Massiv.Array.Manifest.Vector as Massiv
import Data.Monoid (Sum (..))
import Numeric.GSL.Integration
import qualified Numeric.LinearAlgebra as HMat
import RIO hiding (Vector)
import RIO.Partial (fromJust)
import qualified RIO.Vector.Storable as VS

{- | Quadrature integration in the interval \( [a, \infty] \). The function
 must of course converge.
-}
quadInf :: Double -> (Double -> Double) -> (Double, Double)
quadInf a fn = integrateQAGIU 1e-8 1000 fn a

-- | Quadrature integration in the interval \([a, b] \)
quad :: Double -> Double -> (Double -> Double) -> (Double, Double)
quad a b fn = integrateQAGS 1e-8 1000 fn a b

-- | Euclidean distance of two vectors
euclideanDistance ::
  ( Floating e
  , Numeric r e
  , Source r e
  , MonadThrow m
  ) =>
  Vector r e ->
  Vector r e ->
  m e
euclideanDistance a b
  | szA == szB = return . magnitude $ a !-! b
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA = Massiv.size a
  szB = Massiv.size b

-- | Magnitude of a vector
magnitude :: (Floating e, Source r e) => Vector r e -> e
magnitude = sqrt . Massiv.sum . Massiv.map (^ (2 :: Int))

{- | Numerically stable angle between two vectors, see <https://www.jwwalker.com/pages/angle-between-vectors.html>

\[
  \alpha = 2 \operatorname{atan2} (\lVert \lVert \mathbf{v} \rVert \mathbf{u} - \lVert \mathbf{u} \rVert v \rVert,
  \lVert \lVert \mathbf{v} \rVert \mathbf{u} + \lVert \mathbf{u} \rVert v \rVert )
\]
-}
angle ::
  (MonadThrow m, Numeric r e, RealFloat e, Source r e) =>
  Vector r e ->
  Vector r e ->
  m e
angle u v = do
  let magU = magnitude u
      magV = magnitude v
      magVxU = magV *. u
      magUxV = magU *. v
  l <- magVxU .-. magUxV
  r <- magVxU .+. magUxV
  return . (2 *) $ atan2 (magnitude l) (magnitude r)

{- | Distance matrix between two sets of points \( \mathbf{A} \) and
 \( \mathbf{B} \). Points are column vectors of each matrix, e.g.
 \( \mathbf{A} = (\mathbf{a}_1, \dots, \mathbf{a}_n) \).
Consequently, the number of rows must match. The resulting distance matrix will
have a dimension of \( n \times m \) where \(n\) is the number of rows in
 \( \mathbf{A} \) and \(m\) the number of rows in \( \mathbf{B} \).
-}
distMat ::
  forall e m r1 r2.
  (MonadThrow m, Manifest r1 e, Manifest r2 e, Floating e) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Matrix D e)
distMat a b
  | aR == bR = do
      let aB = expandWithin' (Dim 1) (Sz bC) const a :: Array D Ix3 e
          bA = expandWithin' (Dim 2) (Sz aC) const b :: Array D Ix3 e
      Massiv.map (sqrt . getSum)
        . Massiv.foldWithin' (Dim 3)
        . Massiv.map (Sum . (^ (2 :: Int)))
        <$> (aB .-. bA)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (aR :. aC)) = Massiv.size a
  szB@(Sz (bR :. bC)) = Massiv.size b

{- | An Euclidean distance matrix of all points to all other points. A point is
given as a column vector of the input matrix. Equivalent to @'distMat' a a@.
-}
distMat' :: forall e r. (Manifest r e, Floating e) => Matrix r e -> Matrix D e
distMat' a = fromJust $ distMat a a

-- | Root mean square value of an array
rms :: (Floating e, Stream r ix e) => Array r ix e -> e
rms arr = sqrt . (/ fromIntegral n) . Massiv.ssum . Massiv.smap (^ (2 :: Int)) $ arr
 where
  Sz1 n = Massiv.linearSize arr

-- | Dot product of two 'Vector's
(<.>) ::
  ( MonadThrow m
  , HMat.Numeric a
  , Manifest r1 a
  , Manifest r2 a
  , Load r1 Ix1 a
  , Load r2 Ix1 a
  ) =>
  Vector r1 a ->
  Vector r2 a ->
  m a
a <.> b
  | szA == szB = return $ v2hv a HMat.<.> v2hv b
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA = Massiv.size a
  szB = Massiv.size b

-- | Matrix-vector product
(#>) ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Numeric e
  ) =>
  Matrix r1 e ->
  Vector r2 e ->
  m (Vector r3 e)
m #> v
  | c == r = return . hv2v $ hm HMat.#> hv
  | otherwise = throwM $ SizeMismatchException (Sz c) (Sz r)
 where
  hm = m2hm m
  hv = v2hv v
  Sz (_ :. c) = Massiv.size m
  Sz r = Massiv.size v

-- | Matrix-matrix multiplication
(##) ::
  ( Manifest r1 e
  , Manifest r2 e
  , Manifest r3 e
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 e
  , HMat.Numeric e
  , MonadThrow m
  ) =>
  Matrix r2 e ->
  Matrix r1 e ->
  m (Matrix r3 e)
a ## b
  | c == r = return . hm2m $ ha HMat.<> hb
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  ha = m2hm a
  hb = m2hm b
  szA@(Sz (_ :. c)) = Massiv.size a
  szB@(Sz (r :. _)) = Massiv.size b

-- | Make a square diagonal matrix from a vector
diag :: (Load r2 Ix2 e, Manifest r1 e, Num e) => Vector r1 e -> Matrix r2 e
diag v = makeArray Par (Sz $ n :. n) (\(r :. c) -> if r == c then v ! r else 0)
 where
  Sz n = size v

-- | Eigenvalue decomposition.
eig ::
  forall r1 r2 r3 m e.
  ( MonadThrow m
  , Ord (Complex Double)
  , Manifest r1 e
  , Manifest r2 (Complex Double)
  , Manifest r3 (Complex Double)
  , Load r1 Ix1 e
  , Load r2 Ix1 (Complex Double)
  , Load r3 Ix1 (Complex Double)
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  m (Vector r2 (Complex Double), Matrix r3 (Complex Double))
eig m
  | r == c =
      return
        . (orderEigen :: (Vector r2 (Complex Double), Matrix r3 (Complex Double)) -> (Vector r2 (Complex Double), Matrix r3 (Complex Double)))
        . bimap hv2v hm2m
        . HMat.eig
        $ hm
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

-- | Generalised eigenvalue problem.
geig ::
  ( MonadThrow m
  , Manifest r1 e
  , Manifest r2 e
  , Manifest r3 (Complex Double)
  , Manifest r4 e
  , Manifest r5 (Complex Double)
  , Load r1 Ix1 e
  , Load r2 Ix1 e
  , Load r3 Ix1 (Complex Double)
  , Load r4 Ix1 e
  , Load r5 Ix1 (Complex Double)
  , HMat.Field e
  ) =>
  Matrix r1 e ->
  Matrix r2 e ->
  m (Vector r3 (Complex Double), Vector r4 e, Matrix r5 (Complex Double))
geig a b
  | all (== rA) [cA, rB, cB] =
      return
        . (\(alphas, betas, v) -> (hv2v alphas, hv2v betas, hm2m v))
        $ HMat.geig (m2hm a) (m2hm b)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (rA :. cA)) = Massiv.size a
  szB@(Sz (rB :. cB)) = Massiv.size b

{- | Eigenvalue decomposition of a hermitian matrix, which should be symmetrix.
Symmetry is enforced via \( \mathbf{M} + \mathbf{M}^\mathrm{T} / 2 \) !
-}
eigSH ::
  forall r1 r2 r3 m.
  ( MonadThrow m
  , Manifest r1 Double
  , Manifest r2 Double
  , Manifest r3 Double
  , Load r1 Ix1 Double
  , Load r2 Ix1 Double
  , Load r3 Ix1 Double
  , HMat.Field Double
  ) =>
  Matrix r1 Double ->
  m (Vector r2 Double, Matrix r3 Double)
eigSH m
  | r == c =
      return
        . (orderEigen :: (Vector r2 Double, Matrix r3 Double) -> (Vector r2 Double, Matrix r3 Double))
        . bimap hv2v hm2m
        . HMat.eigSH
        . HMat.sym
        $ hm
  | otherwise = throwM $ SizeMismatchException (Sz r) (Sz c)
 where
  Sz (r :. c) = Massiv.size m
  hm = m2hm m

{- | Generalised eigenvalue problem for symmetric hermitian matrices.
The symmetry will be enforced!
-}
geigSH ::
  forall r1 r2 r3 r4 m.
  ( MonadThrow m
  , Ord Double
  , Manifest r1 Double
  , Manifest r2 Double
  , Manifest r3 Double
  , Manifest r4 Double
  , Load r1 Ix1 Double
  , Load r2 Ix1 Double
  , Load r3 Ix1 Double
  , Load r4 Ix1 Double
  , HMat.Field Double
  ) =>
  Matrix r1 Double ->
  Matrix r2 Double ->
  m (Vector r3 Double, Matrix r4 Double)
geigSH a b
  | all (== rA) [cA, rB, cB] =
      return
        . (orderEigen :: (Vector r3 Double, Matrix r4 Double) -> (Vector r3 Double, Matrix r4 Double))
        . bimap hv2v hm2m
        $ HMat.geigSH (HMat.sym hA) (HMat.sym hB)
  | otherwise = throwM $ SizeMismatchException szA szB
 where
  szA@(Sz (rA :. cA)) = Massiv.size a
  szB@(Sz (rB :. cB)) = Massiv.size b
  hA = m2hm a
  hB = m2hm b

{- | Order eigenvalues and eigenvectors in ascending size.
 orderEigen ::
   (Ord e1, Source r2 e2, Manifest r1 e1, Manifest D (e1, Ix1)) =>
   (Vector r1 e1, Matrix r2 e2) ->
   (Vector r1 e1, Matrix r2 e2)
-}
orderEigen ::
  ( Ord e1
  , Unbox e1
  , Manifest r1 e1
  , Manifest r2 e2
  , Source r3 e2
  ) =>
  (Vector r1 e1, Matrix r3 e2) ->
  (Vector r1 e1, Matrix r2 e2)
orderEigen (eVals, eVecs) = (quicksort eVals, compute eVecsOrd)
 where
  eValsOrdAnno = quicksort . compute @U . Massiv.imap (\i e -> (e, i)) $ eVals
  permVec = compute @U . Massiv.imap (\i (_, oI) -> (i, oI)) $ eValsOrdAnno
  szVecs = size eVecs
  eVecsOrd = backpermute' szVecs (\(r :. newC) -> r :. snd (permVec ! newC)) eVecs

-- | Convert a Massiv 'Massiv.Vector' to HMatrix 'HMat.Vector'.
v2hv :: (Manifest r e, Load r Ix1 e, HMat.Element e) => Massiv.Vector r e -> HMat.Vector e
v2hv v = Massiv.toVector v

-- | Convert a Massiv 'Massiv.Matrix' to a HMatrix 'HMat.Matrix'.
m2hm :: (HMat.Element e, Manifest r e, Load r Ix1 e) => Massiv.Matrix r e -> HMat.Matrix e
m2hm mMat = HMat.reshape nCols . v2hv . Massiv.flatten $ mMat
 where
  Sz (_nRows :. nCols) = Massiv.size mMat

-- | Convert a HMatrix 'HMat.Vector' to a Massiv 'Massiv.Vector'.
hv2v :: (HMat.Element e, Manifest r e, Load r Ix1 e) => VS.Vector e -> Massiv.Vector r e
hv2v hVec = Massiv.fromVector' Seq (Sz $ VS.length hVec) hVec

-- | Convert a HMatrix 'HMat.Matrix' to a Massiv 'Massiv.Matrix'.
hm2m :: (Manifest r e, Load r Ix1 e, HMat.Element e) => HMat.Matrix e -> Massiv.Matrix r e
hm2m hMat = Massiv.resize' (Sz $ nRows :. nCols) . hv2v . HMat.flatten $ hMat
 where
  nRows = HMat.rows hMat
  nCols = HMat.cols hMat