# `final` and `prev` refer to the top-level overlay at the nixpkgs level. Thus,
# this overlay can directly applied to nixpkgs.
final: prev:

let
  # Haskell overlay for all GHC versions. `package` refers to the Haskell
  # package defined and build in this repo and should probably be renamed.
  haskellOverrides = hfinal: hprev: {
    simple-scf = hprev.callCabal2nixWithOptions "simple-scf" ../. "" { };
  };
in
{
  haskell = prev.haskell // {
    packages = prev.haskell.packages // (builtins.mapAttrs
      (key: val: val.override { overrides = haskellOverrides; })
      prev.haskell.packages
    );
  };
}
