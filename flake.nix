{
  description = "A nixified Haskell project";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system:
    let
      hsOvl = import ./nix/hsOverlay.nix;

      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [ hsOvl ];
      };

    in
    {
      packages = {
        default = self.packages."${system}".package;
        package = pkgs.haskellPackages.package;
      };

      devShells.default = pkgs.haskellPackages.shellFor {
        withHoogle = true;
        packages = p: [ p.simple-scf ];
        buildInputs = with pkgs; [
          cabal-install
          cabal2nix
          haskell-language-server
          haskellPackages.hls-fourmolu-plugin
          haskellPackages.fourmolu
          hlint
          hpack
          nixpkgs-fmt
        ];
      };

      overlays.default = hsOvl;
    });
}
